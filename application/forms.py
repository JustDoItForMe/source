from wtforms import TextField, PasswordField, SubmitField, BooleanField, SelectField
from flask_wtf import Form
from wtforms.validators import DataRequired

class LoginForm(Form):
    username = TextField('Username', validators=[DataRequired()] )
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Login in')

class SearchForm(Form):
    bookname = TextField('Book name', validators=[DataRequired()])
    search = SubmitField('Try find')

class AddForm(Form):
    name = TextField('Book name', validators=[DataRequired()])
    author = TextField('Author', validators=[DataRequired()])
    add = SubmitField('Add')

class DelForm(Form):
    name = TextField('Book name', validators=[DataRequired()])
    author = TextField('Author', validators=[DataRequired()])
    delete = SubmitField('Delete')
