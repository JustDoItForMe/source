from flask import render_template, redirect, flash, session, url_for, request
from application import appl,db
from .forms import LoginForm, SearchForm, AddForm, DelForm
from flask_login import login_required, login_user, logout_user
from .models import User, Book, Author

@appl.route('/')
@appl.route('/index')
def index():
    return render_template('index.html')

@appl.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
         user = User.query.filter_by(username=form.username.data).first()
         if user is not None and user.verify_password(form.password.data):
             login_user(user, form.remember_me.data)
             return redirect(request.args.get('next') or url_for('index'))
         flash('Invalid username or password')
    return render_template('login.html',
                           form=form)

@appl.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been looged out')
    return redirect(url_for('index'))

@appl.route('/lib', methods=['GET', 'POST'])
def lib():
    form = SearchForm()
    list = Book.query.all()
    if form.validate_on_submit():
        book = Book.query.filter_by(name=form.bookname.data.title()).first()
        if book is not None:
            flash("Book '" + book.name + "' in library with ID:" + str(book.id))
        else:
            flash('Ops, where is book?')
    return render_template('lib.html',
                           books=list, form=form)


@appl.route('/hide', methods=['GET', 'POST'])
@login_required
def secret():
    form = AddForm()
    if form.validate_on_submit():
        book = Book.query.filter_by(name=form.name.data).first()
        if book is None:
            author = Author.query.filter_by(name=form.author.data).first()
            if author is None:
                author = Author(name=form.author.data)
                db.session.add(author)
            book = Book(name=form.name.data)
            book.authors.append(author)
            db.session.add(book)
            db.session.commit()
            flash("Book '" + book.name + "' added to library")
        else:
            flash('This book already in library')
        return redirect(request.args.get('next') or url_for('lib'))
    return render_template('hide.html',
                           form=form)

@appl.route('/del', methods=['GET', 'POST'])
@login_required
def delete():
    form = DelForm()
    if form.validate_on_submit():
        book = Book.query.filter_by(name=form.name.data).first()
        if book is not None and book.authors[0].name == form.author.data:
            db.session.delete(book)
            db.session.commit()
            flash("Book '" + book.name + "' has been deleted")
        else:
            flash('Really?This book was not in library')
        return redirect(request.args.get('next') or url_for('lib'))
    return render_template('del.html',
                    form=form)