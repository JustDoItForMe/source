from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

appl = Flask(__name__)
appl.config.from_pyfile('config.py')

db = SQLAlchemy(appl)

lm = LoginManager()
lm.session_protection = 'strong'
lm.login_view = 'login'
lm.init_app(appl)

from application import views, models