import os

basedir = os.path.abspath(os.path.dirname(__file__))

CSRF_ENABLED = False
SECRET_KEY = 'you-will-never-know'
RECAPTCHA_PUBLIC_KEY = True

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_COMMIT_ON_TEADOWN = True